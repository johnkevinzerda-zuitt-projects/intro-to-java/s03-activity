package com.zerda.s03activity;

import java.util.Scanner;

public class ComputeFactorial {

    public static void main(String[] args) {

        int factorial = 1;
        int ctr = 1;
        int num = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("This is a program that computes a factorial of a number");
        System.out.println("Please input a positive integer: ");

        try{
            num = in.nextInt();
            if (num < 0) {
                System.out.println("Input is a Negative Integer!");
            } else {
                while (ctr <= num) {
                    factorial *= ctr;
                    ctr++;
                }
                System.out.println(String.format("%d! is %d", num, factorial));
            }
        } catch (Exception err1) {
            System.out.println("Input is not an Integer!");
        }
    }
}
